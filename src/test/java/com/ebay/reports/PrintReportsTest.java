package com.ebay.reports;

import org.testng.annotations.Test;

import java.util.Arrays;

import org.testng.annotations.DataProvider;

public class PrintReportsTest {
    @Test(enabled = false, dataProvider = "dp")
    public void printReports(Emp e, int id) {
        PrintReports.printReports(e);
    }

    @Test(enabled = false, dataProvider = "dp")
    public void searchAndPrintReports(Emp e, int id) {
        PrintReports.searchAndPrintReports(e, id);
    }

    @Test(enabled = true, dataProvider = "dp")
    public void searchAndPrintReportsA(Emp e, int id) {
        PrintReports.searchAndPrintReportsA(e, id);
    }

    @Test(enabled = false, dataProvider = "fromString")
    public void mustReadOrgFromStrings(String[] org, int id) {
        System.out.println(Arrays.asList(org).toString());
        Emp e = Emp.readOrg(org);
        System.out.printf("org: %s%n", e);
    }

    @Test(enabled = false, dataProvider = "fromStringInvalidFormat", expectedExceptions = {IllegalArgumentException.class} )
    public void mustReadOrgFromStringsCatchingInvalidFormat(String[] org, int id) {
        System.out.println(Arrays.asList(org).toString());
        Emp e = Emp.readOrg(org);
        System.out.printf("org: %s%n", e);
    }

    @DataProvider
    public Object[][] fromString() {
        return new Object[][] {
                new Object[] { new String[] {"1:", }, 1 },
                new Object[] { new String[] {"1:2,3", "3:4,5", "5:6,7,8", }, 3 },
                new Object[] { new String[] {"5:6,7,8", "1:2,3", "3:4,5", }, 3 },
        };
    }

    @DataProvider
    public Object[][] fromStringInvalidFormat() {
        return new Object[][] {
                new Object[] { new String[] {"", }, 0 },
                new Object[] { new String[] {"1", }, 1 },
        };
    }

    @DataProvider
    public Object[][] dp() {
        return new Object[][] {
                new Object[] { null, 0 },
                new Object[] { new Emp(1), 1 },
                new Object[] { new Emp(1, new Emp[] { new Emp(2), new Emp(3, new Emp[]{new Emp(4), new Emp(5)}) }), 3 },
                new Object[] { new Emp(1, new Emp[] { new Emp(2), new Emp(3, new Emp[]{new Emp(4), new Emp(5)}) }), 1 },
                new Object[] { new Emp(1, new Emp[] { new Emp(2), new Emp(3, new Emp[]{new Emp(4, new Emp[]{new Emp(6)}), new Emp(5)}) }), 3 },
                new Object[] { new Emp(1, new Emp[] { new Emp(2), new Emp(3, new Emp[]{new Emp(4, new Emp[]{new Emp(6)}), new Emp(5)}) }), 1 },
        };
    }

}
