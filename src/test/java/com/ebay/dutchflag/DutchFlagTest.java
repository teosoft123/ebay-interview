package com.ebay.dutchflag;

import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

import org.testng.annotations.DataProvider;
import static com.ebay.dutchflag.DutchColors.*;

public class DutchFlagTest {

    @DataProvider
    public Object[][] data() {
        return new Object[][] {
            new Object[] { new DutchColors[] { RED }, new DutchColors[] { RED } },
            new Object[] { new DutchColors[] { RED, BLUE, WHITE }, new DutchColors[] { RED, WHITE, BLUE } },
            new Object[] { new DutchColors[] { BLUE, RED, WHITE }, new DutchColors[] { RED, WHITE, BLUE } },
            new Object[] { new DutchColors[] { BLUE, RED, WHITE }, new DutchColors[] { RED, WHITE, BLUE } },
            };
    }

    @DataProvider
    public Object[][] baddata() {
        return new Object[][] {
            new Object[] { new DutchColors[] { BLUE, RED, WHITE, BAD_COLOR }, null },
            };
    }

    @Test(dataProvider = "data")
    public void mustSort(DutchColors[] source, DutchColors[] sorted) {
        DutchFlag.sort(source);
        assertEquals(source, sorted);
    }


    @Test(dataProvider = "baddata", expectedExceptions = { IllegalArgumentException.class })
    public void mustFailOnBadColor(DutchColors[] source, DutchColors[] sorted) {
        DutchFlag.sort(source);
        assertEquals(source, sorted);
    }

}
