package com.ebay.dutchflag;

public class DutchFlag {

    static void swap(DutchColors[] src, int i, int j) {
        DutchColors tmp = src[i];
        src[i] = src[j];
        src[j] = tmp;
    }

    public static void sort(DutchColors[] src) {
        int lo = 0, mid = 0;
        int hi = src.length - 1;
        while(mid <= hi) {
            switch(src[mid]) {
            case RED:
                swap(src, lo++, mid++);
                continue;
            case WHITE:
                mid++;
                continue;
            case BLUE:
                swap(src, mid, hi--);
                continue;
            default:
                throw new IllegalArgumentException(); // won't happen with enum
            }
        }
    }

}
