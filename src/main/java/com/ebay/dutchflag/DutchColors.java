package com.ebay.dutchflag;

public enum DutchColors {
    RED,
    WHITE,
    BLUE,
    BAD_COLOR,
}
