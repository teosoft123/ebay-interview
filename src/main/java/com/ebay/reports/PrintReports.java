package com.ebay.reports;

public class PrintReports {

    public static void searchAndPrintReports(Emp e, int id) {
        if (e != null) {
            if (e.id == id) {
                printReports(e);
                System.out.println();
            } else {
                for (Emp r : e.reps) {
                    searchAndPrintReports(r, id);
                }
            }
        }
    }

    public static void searchAndPrintReportsA(Emp e, int id) {
        Emp found = Emp.findChild(e, new Emp(id));
        if(null != found) {
            printReports(found);
        }
    }

    public static void printReports(Emp e) {
        System.out.printf("%d%s", e.id, e.reps.isEmpty() ? "," : "");
        printBracketCond(e, "[");
        for (Emp r : e.reps) {
            printReports(r);
        }
        printBracketCond(e, "]");
    }

    private static void printBracketCond(Emp e, String bracket) {
        if(!e.reps.isEmpty()) {
            System.out.print(bracket);
        }

    }

}
