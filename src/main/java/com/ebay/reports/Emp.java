package com.ebay.reports;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.mutable.MutableBoolean;

public class Emp {
    private static final Emp VOID_ORG = new Emp(0, new Emp[0]); // no boss, no reports
    final int id;
    final Set<Emp> reps = new HashSet<>();

    public Emp(int id) {
        this.id = id;
    }

    public Emp(int id, Emp... reports) {
        this.id = id;
        this.reps.addAll(Arrays.asList(reports));
    }

    /**
     * @param org Records in form of int[] represent a boss with reports, where id at index 0 is a
     *        boss, remaining ids are reports
     * @return Emp == CEO == Org Records come in arbitrary order, and we need to check if employees
     *         with incoming ids are already in the tree
     */
    public static Emp readOrg(String...org) {
        Emp organization = null;
        Emp e = createFromString(org[0]);
        Emp.insertChild(organization, e);
        return organization;
    }

    private static void insertChild(Emp organization, Emp e) {
        Emp c = findChild(organization, e);

    }

    public static Emp findChild(Emp organization, Emp e) {
        if(null == e) {
            return null;
        }
        if(null == organization) {
            organization = e; // if later on this Emp is found to be a report for
            return e;         // anyone the tree gets a new root and has to be modified accordingly
        } else {
            return safeFindChild(organization, e);
        }
    }

    public static Emp safeFindChild(Emp startNode, Emp e) {
        if (startNode.id == e.id) {
            return startNode;
        } else {
            for (Emp r : startNode.reps) {
                if(null != r) { // Set in general allows null elements
                    Emp found = safeFindChild(r, e);
                    if(null != found) {
                        return found;
                    }
                }
            }
        }
        return null;
    }



    /**
     * @param empWithReps format bossId:report,report...
     * @return Emp boss with reports
     * I feel like I'll end up with using full-blown regex-based parser if I add things like names in the source data
     * In fact, it kind of becomes a CSV-type format, so I can just as well use CSV parser :)
     */
    private static Emp createFromString(String empWithReps) {
        String[] e = empWithReps.split(":\\s*?");
        if(e.length == 1) {
            if(System.identityHashCode(empWithReps) == System.identityHashCode(e[0])) {
                throw new IllegalArgumentException(String.format("Invalid format: %s", empWithReps));
            }
            // has employee only
            return new Emp(Integer.parseInt(e[0]));
        }
        // else has reports
        return new Emp(Integer.parseInt(e[0]), parseReports(e));
    }

    private static Emp[] parseReports(String[] e) {
        String[] reps = e[1].split(",\\s*?");
        Emp[] reports = new Emp[reps.length];
        Arrays.setAll(reports, i -> new Emp(Integer.parseInt(reps[i])));
        return reports;
    }

    @Override
    public String toString() {
        return "Emp [id=" + id + ", reps=" + reps + "]";
    }

}
